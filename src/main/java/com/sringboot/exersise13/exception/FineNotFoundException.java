package com.sringboot.exersise13.exception;

public class FineNotFoundException extends Exception {
    public FineNotFoundException(String message) {
        super(message);
    }
}
