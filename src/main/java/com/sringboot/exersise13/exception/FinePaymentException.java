package com.sringboot.exersise13.exception;

public class FinePaymentException extends Exception {
    public FinePaymentException(String message) {
        super(message);
    }
}
