package com.sringboot.exersise13.exception;

public class FineToCourtException extends Exception {
    public FineToCourtException(String message) {
        super(message);
    }
}
