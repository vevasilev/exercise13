package com.sringboot.exersise13.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Fine {

    private Integer id;
    private String carNumber;
    private String intruder;
    private String inspector;
    private LocalDate timeProtocol;
    private Boolean subpoena;
    private Boolean paymentFine;
    private LocalDate datePaymentFine;
    private LocalDate dateDeadlinePaymentFine;

    public Fine() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getIntruder() {
        return intruder;
    }

    public void setIntruder(String intruder) {
        this.intruder = intruder;
    }

    public String getInspector() {
        return inspector;
    }

    public void setInspector(String inspector) {
        this.inspector = inspector;
    }

    public LocalDate getTimeProtocol() {
        return timeProtocol;
    }

    public void setTimeProtocol(String timeProtocol) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        this.timeProtocol = timeProtocol == null
                ? null
                : LocalDate.parse(timeProtocol, formatter);
    }

    public Boolean getSubpoena() {
        return subpoena;
    }

    public void setSubpoena(Boolean subpoena) {
        this.subpoena = subpoena;
    }

    public Boolean getPaymentFine() {
        return paymentFine;
    }

    public void setPaymentFine(Boolean paymentFine) {
        this.paymentFine = paymentFine;
    }

    public LocalDate getDatePaymentFine() {
        return datePaymentFine;
    }

    public void setDatePaymentFine(String datePaymentFine) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        this.datePaymentFine = datePaymentFine == null
                ? null
                : LocalDate.parse(datePaymentFine, formatter);
    }

    public LocalDate getDateDeadlinePaymentFine() {
        return dateDeadlinePaymentFine;
    }

    public void setDateDeadlinePaymentFine(String dateDeadlinePaymentFine) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        this.dateDeadlinePaymentFine = dateDeadlinePaymentFine == null
                ? null
                : LocalDate.parse(dateDeadlinePaymentFine, formatter);
    }
}