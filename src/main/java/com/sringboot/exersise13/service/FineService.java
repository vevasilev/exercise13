package com.sringboot.exersise13.service;

import com.sringboot.exersise13.exception.FineNotFoundException;
import com.sringboot.exersise13.exception.FinePaymentException;
import com.sringboot.exersise13.exception.FineToCourtException;
import com.sringboot.exersise13.model.Fine;

import java.util.List;

public interface FineService {

    List<Fine> getAllFines();

    void addFine(Fine fine);

    void updateFine(Fine fine) throws FineNotFoundException;

    Fine deleteFine(Integer id) throws FineNotFoundException;

    Fine getFine(Integer id) throws FineNotFoundException;

    void payFine(Integer id) throws FineNotFoundException, FinePaymentException;

    void fineToCourt(Integer id) throws FineNotFoundException, FineToCourtException;
}
