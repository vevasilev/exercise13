package com.sringboot.exersise13.dao;

import com.sringboot.exersise13.model.Fine;
import java.util.List;

public interface FineDAO {

    void addFine(Fine fine);

    void updateFine(Fine fine);

    Fine deleteFine(Integer id);

    Fine getFine(Integer id);

    List<Fine> getAllFines();

    void payFine(Integer id);

    void fineToCourt(Integer id);
}
