package com.sringboot.exersise13.dao;

import com.sringboot.exersise13.model.Fine;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class FineDAOImpl implements FineDAO {

    private static final HashMap<Integer, Fine> FINE_REPOSITORY_HASHMAP = new HashMap<>();
    private static final AtomicInteger FINE_ID_HOLDER = new AtomicInteger();

    @Override
    public void addFine(Fine fine) {
        final int id = FINE_ID_HOLDER.incrementAndGet();
        fine.setId(id);
        FINE_REPOSITORY_HASHMAP.put(id, fine);
    }

    @Override
    public void updateFine(Fine fine) {
        FINE_REPOSITORY_HASHMAP.put(fine.getId(), fine);
    }

    @Override
    public Fine deleteFine(Integer id) {
        return FINE_REPOSITORY_HASHMAP.remove(id);
    }

    @Override
    public Fine getFine(Integer id) {
        return FINE_REPOSITORY_HASHMAP.get(id);
    }

    @Override
    public List<Fine> getAllFines() {
        return new ArrayList<>(FINE_REPOSITORY_HASHMAP.values());
    }

    @Override
    public void payFine(Integer id) {
        FINE_REPOSITORY_HASHMAP.get(id).setPaymentFine(true);
        FINE_REPOSITORY_HASHMAP.get(id).setDatePaymentFine(
                new SimpleDateFormat("dd.MM.yyyy").format(new Date())
        );
    }

    @Override
    public void fineToCourt(Integer id) {
        FINE_REPOSITORY_HASHMAP.get(id).setSubpoena(true);
    }
}
