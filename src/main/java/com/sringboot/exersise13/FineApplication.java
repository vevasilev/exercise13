package com.sringboot.exersise13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FineApplication {

    public static void main(String[] args) {
        SpringApplication.run(FineApplication.class, args);
    }

}